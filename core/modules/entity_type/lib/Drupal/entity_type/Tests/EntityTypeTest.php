<?php

/**
 * @file
 * Definition of Drupal\field\Tests\FieldTestBase.
 */

namespace Drupal\entity_type\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\entity_type\Plugin\Core\Entity\EntityType;

class EntityTypeTest extends WebTestBase {
  
  function setUp() {
    parent::setUp();
  }

  public static function getInfo() {
    return array(
      'name' => 'Entity type tests',
      'description' => 'Test for dynamic entity types.',
      'group' => 'Entity Type',
    );
  }
  
  function testEntityType() {
    $et = new EntityType(array('name' => "fake"), "entity_type");
    $et->save();
  }
}

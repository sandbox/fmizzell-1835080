<?php
namespace Drupal\entity_type\Plugin\Core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

use Drupal\entity_type\Plugin\Core\Entity\EntityStorageManagerInterface;

/**
 * @Plugin(
 *   id = "entity_type",
 *   label = @Translation("Entity Type"),
 *   module = "entity_type",
 *   controller_class = "Drupal\entity_type\Plugin\Core\Entity\EntityTypeStorageController",
 *   config_prefix = "entity_type",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "human_name",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class EntityType extends ConfigEntityBase {
  
  /**
   * The machine name that the entity type will use.
   */
  public $name;
  
  public $manager_class;
  
  private $manager;


  /**
   * Constructs an Entity Type object.
   */
  public function __construct($values = array(), EntityStorageManagerInterface $m){
    parent::__construct($values, "entity_type");
  }
  
  /**
   * 
   */
  public function id() {
    return isset($this->name) ? $this->name : NULL;
  }
  
  /**
   * Create the entity type table, if it has not been created yet
   */
  public function save(){
    $this->initializeManager();
    $this->manager->initialize();
    parent::save();
  }
  
  /**
   * Delete the entity type table
   */
  public function delete(){
    $this->initializeManager();
    $this->manager->destroy();
    parent::delete();
  }
  
  private function initializeManager(){
    if(!$this->manager){
      $this->manager = new $this->manager_class($this);
    }
  }
}


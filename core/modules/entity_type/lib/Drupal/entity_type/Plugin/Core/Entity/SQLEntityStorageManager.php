<?php
namespace Drupal\entity_type\Plugin\Core\Entity;

use Drupal\entity_type\Plugin\Core\Entity\EntityStorageManagerInterface;

class SQLEntityStorageManager implements EntityStorageManagerInterface {
  
  private $entity_type;
  
  public function __construct(EntityType $entity_type){
    $this->entity_type = $entity_type;
  }
  
  public function initialize() {
    $id = $this->entity_type->id();
    if($id && !db_table_exists($id)){
      db_create_table($id, entity_type_default_schema());
    }
  }
  
  public function destroy() {
    $id = $this->entity_type->id();
    db_drop_table($id);
  }
}

?>

<?php
namespace Drupal\entity_type\Plugin\Core\Entity;
use Drupal\Core\Config\Entity\ConfigStorageController;

class EntityTypeStorageController extends ConfigStorageController {
  public function __construct() {
    parent::__construct('entity_type');
  }
}

?>

<?php

/**
 * @file
 * Definition of Drupal\Core\Entity\EntityTypeInfo.
 */

namespace Drupal\entity_type\Plugin\Core\Entity;

use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 *
 * @Plugin(
 *   id = "entity_type_info",
 *   module = "entity_type",
 *   derivative = "Drupal\entity_type\Plugin\Core\Entity\EntityTypeDerivatives"
 * )
 */

class EntityTypeInfo{

}

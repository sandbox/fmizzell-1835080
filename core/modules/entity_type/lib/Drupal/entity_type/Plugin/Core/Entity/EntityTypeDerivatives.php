<?php

/**
 * @file
 * Definition of Drupal\Core\Entity\EntityTypeDerivatives.
 */

namespace Drupal\entity_type\Plugin\Core\Entity;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;
use Drupal\Core\Config\Entity\ConfigStorageController;

/**
 * Defines EntityTypeDerivatives.
 */
class EntityTypeDerivatives implements DerivativeInterface {
  
  /**
   * Gets a plugin definition.
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    $derivatives = $this->getDerivativeDefinitions($base_plugin_definition);
    if (isset($derivatives[$derivative_id])) {
      return $derivatives[$derivative_id];
    }
  }

  /**
   * Gets EntityType configs and creates plugin definitions.
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    $derivatives = array();
    $entity_types = config_get_storage_names_with_prefix('entity_type');
    foreach($entity_types as $et){
      $obj = config($et);
      $name = $obj->get("name");
      
      $derivatives[$name] = array(
        'id' => 'id',
        'label' => $name,
        'base_table' => $name,
      ) + $base_plugin_definition;
    }
    return $derivatives;
  }
}


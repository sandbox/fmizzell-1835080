<?php

namespace Drupal\entity_type\Plugin\Core\Entity;

use Drupal\entity_type\Plugin\Core\Entity\EntityType;
/**
 * When an entity type is create, we also need to to initialize the storage
 * that will be used for entities of the created entity type. Some storage
 * controller make this assumption and that is why we have to take care of this
 */
interface EntityStorageManagerInterface {
  
  public function __construct(EntityType $entity_type);
  /**
   * Initialize an entity storage
   */
  public function initialize();
  
  /*
   * Delete an entity storage
   */
  public function destroy();
}

?>
